const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

let app = express();

// MongoDB
const mongoUri = "mongodb+srv://totesfortutorials:totesfortutorials@cluster0.whqxa.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
})
.then(() => {
    console.log('DB CONNECTED');
})
.catch(e => console.log(`DB CONNECTION ERROR: ${e}`));

// Middlewares and routes
app.use(cors());

// Open port
const port = 8000;
app.listen(port, () => console.log(`Server listening on port ${port}`));