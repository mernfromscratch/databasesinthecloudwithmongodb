
const Form = ({game, setGame, propertiesAndTypesGames, sendToBackend, setHidden}) => {

    const send = (e) => {
        e.preventDefault();
        sendToBackend(game);
    }

    const cancel = (e) => {
        e.preventDefault();
        setHidden(true);
    };

    const makeInputField = (inputDescription) => {
        const inputType = inputDescription.inputType;
        const propertyName = inputDescription.propertyName;
        const setItemCorrectly = (event) => {
            (inputType === 'checkbox') 
            ? setGame({...game, [propertyName]: event.target.checked})
            : setGame({...game, [propertyName]: event.target.value})
        };
        return (
            <tr key={propertyName}>
                <td style={{textAlign: 'left'}}>
                    {propertyName}
                </td>
                <td>
                    <input
                        type={inputType} 
                        value={game[propertyName]} 
                        onChange={setItemCorrectly} 
                        min={inputDescription.min} 
                        max={inputDescription.max} 
                        checked={(inputType === 'checkbox') ? (game[propertyName]) : false}
                    />
                </td>
            </tr>
        )
    };

    return (
        <form>
            <table>
                <tbody>
                {propertiesAndTypesGames !== undefined && propertiesAndTypesGames.map((description) => (
                    makeInputField(description)
                ))}
                </tbody>
            </table>
            <div>
                <button onClick={send}>
                    Submit
                </button>
                <button onClick={cancel}>
                    Cancel
                </button>
            </div>
        </form>
    )
};

export default Form;