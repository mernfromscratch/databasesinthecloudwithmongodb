import {Switch, Route} from 'react-router-dom';
import Home from './Home';
import Edit from './Edit';
import Create from './Create';

import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/" component={Home} />
        <Route path="/edit/:id" component={Edit} />
        <Route path="/create/" component={Create} />
      </Switch>
    </div>
  );
}

export default App;
