import Form from './Form';

const Edit = ({game, setGame, propertiesAndTypesGames, sendToBackend, setHidden}) => {
    return (
        <>
            <Form game={game} setGame={setGame} propertiesAndTypesGames={propertiesAndTypesGames} sendToBackend={sendToBackend} setHidden={setHidden} />
        </>
    );
}

export default Edit;