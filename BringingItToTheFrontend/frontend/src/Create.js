import Form from './Form';

const Create = ({game, setGame, propertiesAndTypesGames, sendToBackend, setHidden}) => {
    return (
        <>
            <Form game={game} setGame={setGame} propertiesAndTypesGames={propertiesAndTypesGames} sendToBackend={sendToBackend} setHidden={setHidden} />
        </>
    );
}

export default Create;