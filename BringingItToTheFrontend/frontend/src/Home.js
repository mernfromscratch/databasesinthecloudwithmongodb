import {useState, useEffect} from 'react';
import List from './List';
import Create from './Create';

const Home = () => {
    const [games, setGames] = useState([]);
    const [hiddenCreate, setHiddenCreate] = useState(true);
    const [hiddenEdit, setHiddenEdit] = useState(true);
    const [gameToCreate, setGameToCreate] = useState({Name: "", Brand: "", Price: 0, Replayability: 50, Genre: "", BloodAndGore: false});
    const propertiesGames = ['Name', 'Brand', 'Price', 'Replayability', 'Genre', 'Blood And Gore', 'Actions'];
    const propertiesAndTypesGames = [
        {propertyName: 'Name', inputType: 'text'}, 
        {propertyName: 'Brand', inputType: 'text'}, 
        {propertyName: 'Price', inputType: 'number'}, 
        {propertyName: 'Replayability', inputType: 'range'}, 
        {propertyName: 'Genre', inputType: 'text'},
        {propertyName: 'BloodAndGore', inputType: 'checkbox'}];
        const styleContainer = { 
            zIndex: '9', 
            position: 'absolute', 
            margin: '0',
            top: '30%',
            bottom: '45%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            padding: '10px',
            backgroundColor: 'white',
            border: '1px solid #AAAAAA',
            borderRadius: '10px'
        };
    

    useEffect(() => {
        fetch("http://localhost:8000/api/games/all")
        .then((res) => res.json())
        .then((allGames) => {
            setGames(allGames);
        });
    }, []);

    useEffect(() => {}, [games]);

    const sendNewGameToBackend = (game) => {
        const createUrl = "http://localhost:8000/api/game/";
        console.log(game);
        fetch(createUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(game)
        })
        .then((res) => res.json())
        .then((newGame) => {
            fetch("http://localhost:8000/api/games/all")
            .then((res) => res.json())
            .then((allGames) => {
                setGames(allGames);
            });
        })
    };

    const popUpToCreate = () => {
        setGameToCreate({Name: "", Brand: "", Price: 0, Replayability: 50, Genre: "", BloodAndGore: false});
        setHiddenCreate(!hiddenCreate);
        setHiddenEdit(true);
    }

    return (
        <div>
            <div style={{...styleContainer, visibility: (hiddenCreate ? "hidden" : "visible") }}>
                <Create game={gameToCreate} setGame={setGameToCreate} propertiesAndTypesGames={propertiesAndTypesGames} sendToBackend={sendNewGameToBackend} setHidden={setHiddenCreate}/>
            </div>
            <button style={{margin: '20px'}} onClick={() => popUpToCreate()}>
                Add a game
            </button>
            <List 
                games={games} 
                setGames={setGames}
                hiddenEdit={hiddenEdit}
                setHiddenEdit={setHiddenEdit}
                setHiddenCreate={setHiddenCreate}
                propertiesGames={propertiesGames}
                propertiesAndTypesGames={propertiesAndTypesGames}
            />
        </div>
    );
}

export default Home;