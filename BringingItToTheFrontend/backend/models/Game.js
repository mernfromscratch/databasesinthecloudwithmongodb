const constants =  require('../constants');

const mongoose = require('mongoose');

const gameSchema = new mongoose.Schema({
    Name: {
        type: String,
        unique: true
    },
    Brand: String,
    Price: Number,
    Replayability: {
        type: Number,
        min: 0,
        max: 100
    },
    Genre: {
        type: String,
        enum: constants.gameGenres
    },
    BloodAndGore: Boolean
});

module.exports = mongoose.model("Game", gameSchema);