const Game = require('../models/Game');

// exports.getAllGames = (req, res) => {
//     res.json([1, 3, 5]);
// };

//  /api/games/all
exports.getAll = async (req, res) => {
    const allGames = await Game.find({})
        .sort([['Name', 'desc']])
        .exec();
    res.json(allGames);
}

//  /api/game/1
exports.getOne = async (req, res) => {
    try {
        const game = await Game.findById(req.params._id).exec();
        res.json(game);            
    } catch (error) {
        res.status(400).send(`No game with id ${req.params._id} was found`);
    }
}

//  /api/game/
exports.createOne = async (req, res) => {
    try {
        const newGame = await new Game({...req.body}).save();
        res.json(newGame);
    } catch (error) {
        res.status(400).send('Could not create new game');
    }
}

//  /api/game/1
exports.editOne = async (req, res) => {
    try {
        const editedGame = await Game.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}).exec();
        res.json(editedGame);
    } catch (error) {
        res.status(400).send(`Error updating game with id ${req.params._id}`);
    }
}

//  /api/game/1
exports.deleteOne = async (req, res) => {
    try {
        const deletedGame = await Game.findOneAndDelete({_id: req.params._id}).exec();
        res.json(deletedGame);
    } catch (error) {
        res.status(400).send(`Could not delete game with id ${req.params._id}`).exec();
    }
}