const express = require('express');
const router = express.Router();

const { getAll,
        getOne,
        createOne,
        editOne,
        deleteOne
} = require('../controllers/games');

// app.get('/api/games/all', (req, res) => {
//     res.json([1, 3, 5]);
// })


router.get('/games/all', getAll);
router.get('/game/:_id', getOne);
router.post('/game/', createOne);
router.put('/game/:_id', editOne);
router.delete('/game/:_id', deleteOne);


module.exports = router;